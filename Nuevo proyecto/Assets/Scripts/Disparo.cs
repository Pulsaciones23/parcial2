﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparo : MonoBehaviour
{
    public Transform spawnBala;
    public GameObject Bullet;
    public GameObject Particulas_Bala;
    
    public float shotForce = 1500;
    public float shotRate = 0.5f;

    private float shotRateTime = 0;

    public AudioSource controlSonido;
    public AudioClip SonidoDeDisparo;


    void Update()
    {

        if (Input.GetButtonDown("Fire1"))
        {
            controlSonido.PlayOneShot(SonidoDeDisparo);
            Instantiate(Particulas_Bala, spawnBala.position, Quaternion.identity);

            if (Time.time>shotRateTime)
            {
                GameObject newBullet;

                newBullet = Instantiate(Bullet,spawnBala.position,spawnBala.rotation);

                newBullet.GetComponent<Rigidbody>().AddForce(spawnBala.forward * shotForce);

                shotRateTime = Time.time + shotRate;

                Destroy(newBullet, 2);

            }
        }

        

    }
}
