﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void CargaNivel(string nombreNivel)
    {
        SceneManager.LoadScene(nombreNivel);
    }

}
