﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaEnemigo : MonoBehaviour
{
    
    private int hp;

    void Start()
    {
        hp = 100;
    }
    
    public void recibirDaño()
    {
        hp = hp - 50;

        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala") == true)
        {
            recibirDaño();
            Debug.Log("La bala pego");
        }
    }

}

