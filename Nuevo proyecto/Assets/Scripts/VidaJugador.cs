﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VidaJugador : MonoBehaviour
{
    public float vida = 100;

    public Image BarraDeVida;


    void Update()
    {
        vida = Mathf.Clamp(vida, 0, 100);

        BarraDeVida.fillAmount = vida / 100;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Bala") == true)
        {
            vida -= 25;
            if (vida == 0)
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
        }

        if (other.gameObject.CompareTag("ESPADA") == true)
        {
            vida -= 50;
            if (vida == 0)
            {
                UnityEditor.EditorApplication.isPlaying = false;
            }
        }
    }
}
