﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VidaBossFinal : MonoBehaviour
{

    private int hp;

    void Start()
    {
        hp = 1000;
    }

    public void recibirDaño()
    {
        hp = hp - 50;

        if (hp <= 0)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Bala") == true)
        {
            recibirDaño();
        }
    }

}
