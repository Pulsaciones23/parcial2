﻿using UnityEngine.SceneManagement;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{
    public Camera camaraPrimeraPersona;

    public float rot = 200.0f;
    public float movimiento = 5.0f;
    private Animator ani;
    public float x, y;


    public int hp = 100;

    //variables salto 
    public Rigidbody rb;
    public bool onGround;
    private Vector3 jump;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        rb = GetComponent<Rigidbody>();
        jump = new Vector3(0.0f, 3.0f, 0.0f);

        ani = GetComponent<Animator>();
    }

    //void para verificar el piso
    void OnCollisionStay()
    {
        onGround = true;
    }

    //void para reiniciar nivel
    void reiniciarNivel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("WIN") == true)
        {
            UnityEditor.EditorApplication.isPlaying = false;
        }
    }



    void Update()
    {

        x = Input.GetAxis("Horizontal");
        y = Input.GetAxis("Vertical");
        //transform.Rotate(0, x * Time.deltaTime * rot, 0);
        transform.Translate(x * Time.deltaTime * movimiento, 0 , y * Time.deltaTime * movimiento);
        ani.SetFloat("velx", x);
        ani.SetFloat("vely", y);



        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        // salto y doble salto
        if (Input.GetKeyDown("space") && onGround)
        {
            rb.AddForce(jump * 2.0f, ForceMode.Impulse);
            onGround = false;
        }

        //reinicio de nivel
        if (Input.GetKey("r"))
        {
            reiniciarNivel();
        }

        if (rb.position.y < -2f)
        {
            reiniciarNivel();
        }
        

    }

}

